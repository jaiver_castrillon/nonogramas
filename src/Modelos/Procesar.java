/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.sort;
import java.util.LinkedList;
import java.util.Comparator;

/**
 *
 * @author alda1
 */
public class Procesar {
    /*Nonograma nonograma;
     public Procesar(Nonograma nonograma) {
     this.nonograma=nonograma;
        
     }*/

    public void resolver(Nonograma nonograma){
        int tamPoblacion = 1000;
        ArrayList<Individuo> poblacion = new ArrayList<>();
        Individuo x;
        LinkedList<LinkedList<LinkedList<Integer>>> list;
        boolean sol_encon = false;
        int elegidos = 0;

        for (int i = 0; i < tamPoblacion; i++) {
            x = (new Individuo(new String[1][1])).crearGenoma(nonograma.getMatriz().length, nonograma.getMatriz()[0].length);
            list = (new AnalizadorDeEstructuras()).AnalizarMatriz(x.getCromosoma());
            x.setAptitud(CacularAptitudIndividuos(nonograma, list.get(0), list.get(1)));
            poblacion.add(x);
        }

        
        ArrayList<Individuo> nuevageneracion;
        Individuo padre;
        Individuo madre;
        
        int generacion=0;
        while (!sol_encon) {
            Collections.sort(poblacion);
            if (poblacion.get(0).getAptitud() <= 0) {
                sol_encon = true;
                break;
            }
            elegidos = (int) ((10 * tamPoblacion) / 100);
            nuevageneracion=new ArrayList<>();
            selecionMejores(poblacion, nuevageneracion, elegidos);
            /*elegidos = (int) ((90 * tamPoblacion) / 100);*/

            for (int i = 0; i < elegidos; i++) {
                for (int j = 0; j < elegidos; j++) {
                    padre = poblacion.get(i);
                    madre = poblacion.get(j);
                    x = padre.reproducir(madre.getCromosoma());
                    list = (new AnalizadorDeEstructuras()).AnalizarMatriz(x.getCromosoma());
                    x.setAptitud(CacularAptitudIndividuos(nonograma, list.get(0), list.get(1)));
                    nuevageneracion.add(x);
                }
            }

            /*for (int i = 0; i < elegidos; i++) {
             padre=poblacion.get((int)(Math.random()*(poblacion.size()/2)));
             madre=poblacion.get((int)(Math.random()*(poblacion.size()/2)));
             x=padre.reproducir(madre.getCromosoma());
             list = (new AnalizadorDeEstructuras()).AnalizarMatriz(x.getCromosoma());
             x.setAptitud(CacularAptitudIndividuos(nonograma, list.get(0), list.get(1)));
             nuevageneracion.add(x);
             }*/
            for (int i = 0; i < elegidos; i++) {
                padre=poblacion.get((int)(Math.random()*(poblacion.size()/2)));
                madre=poblacion.get((int)(Math.random()*(poblacion.size()/2)));
                x=padre.reproducir(madre.getCromosoma());
                list = (new AnalizadorDeEstructuras()).AnalizarMatriz(x.getCromosoma());
                x.setAptitud(CacularAptitudIndividuos(nonograma, list.get(0), list.get(1)));
                nuevageneracion.add(x);
            }
            poblacion=nuevageneracion;
            generacion+=1;

            impmatriz(poblacion.get(0).getCromosoma());
            System.out.println("-----------------------------------------------");
            System.out.println(poblacion.get(0).getAptitud());
            System.out.println(generacion);
        }

        //System.out.println(poblacion.get(0).getAptitud());
        
        nonograma.setMatriz(poblacion.get(0).getCromosoma());
    }

    public static void selecionMejores(ArrayList<Individuo> poblacion, ArrayList<Individuo> generacion, int n) {
        for (int i = 0; i < n; i++) {
            generacion.add(poblacion.get(i));
        }
    }

    
    public static void imprimirListInd(ArrayList<Individuo> pob)
    {
        String cad="";
        for (int i = 0; i < pob.size(); i++) {
            cad+="| "+pob.get(i).getAptitud()+" |";
        }
        System.out.println(cad);
    
    }
    
    public static void impmatriz(String[][] m) {
        String cad = "";
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                if (m[i][j].equals("X")) {
                    cad += m[i][j];
                } else {
                    cad += "O";
                }
            }
            System.out.println(cad);
            cad = "";
        }

    }

    public static int CacularAptitudIndividuos(Nonograma nonograma, LinkedList<LinkedList<Integer>> reglasFil, LinkedList<LinkedList<Integer>> reglasCol) {
        int aptitud = 0;

        for (int i = 0; i < nonograma.getReglasFil().size(); i++) {
            if (!CompararListas(nonograma.getReglasFil().get(i), reglasFil.get(i))) {
                aptitud += 1;
            }
        }
        for (int i = 0; i < nonograma.getReglasCol().size(); i++) {
            if (!CompararListas(nonograma.getReglasCol().get(i), reglasCol.get(i))) {
                aptitud += 1;
            }
        }

        return aptitud;
    }

    public static boolean CompararListas(LinkedList<Integer> list1, LinkedList<Integer> list2) {
        if (list1.size() == list2.size()) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i) != list2.get(i)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

}
