/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.LinkedList;

/**
 *
 * @author alda1
 */
public class AnalizadorDeEstructuras {
    
     public LinkedList<Integer> StrinArrList(String[] s) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 0; i < s.length; i++) {
            list.add(Integer.parseInt(s[i]));
        }
        return list;
    }
    
     public void formatearMatriz(Nonograma nonograma){
        String[][] matriz = new String[nonograma.getMatriz().length][nonograma.getMatriz()[0].length];
        
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (nonograma.getMatriz()[i][j].equalsIgnoreCase("O")) {
                    matriz[i][j] = "";
                }else{
                    matriz[i][j] = nonograma.getMatriz()[i][j];
                }
            }
        }
        nonograma.setMatriz(matriz);
    }
    
    public String[] partir(String linea) {
        String[] arr = new String[linea.length()];
        for (int i = 0; i < linea.length(); i++) {
            arr[i] = "" + linea.charAt(i);
        }
        return arr;
    }
    
    
    public String[][] convListMatriz(LinkedList<String[]> list) {
        String[][] matriz = new String[list.size()][list.get(0).length - 1];
        for (int i = 0; i < list.size(); i++) {
            matriz[i] = list.get(i);
        }
        
        return matriz;
    }
    
     public LinkedList<LinkedList<LinkedList<Integer>>> AnalizarMatriz(String[][] m) {
        LinkedList<LinkedList<Integer>> reglasFil=new LinkedList<>();
        LinkedList<LinkedList<Integer>> reglasCol=new LinkedList<>();
        LinkedList<LinkedList<LinkedList<Integer>>> reglas=new LinkedList<>();
         
        String[] arr = new String[m.length];
        for (int i = 0; i < m.length; i++) {
            
           // System.out.println("fil:" + i + ":" + ExtraerReglas(m[i]));
            reglasFil.add(ExtraerReglas(m[i]));
            for (int j = 0; j < m.length; j++) {
                arr[j] = m[j][i];
             //   System.out.println(arr[j]);
            }
            reglasCol.add(ExtraerReglas(arr));
         //   System.out.println("col:" + i + ":" + ExtraerReglas(arr));
        }
        reglas.add(reglasFil);
        reglas.add(reglasCol);
        return reglas;
    }
    
    public LinkedList<Integer> ExtraerReglas(String[] arr) {
        LinkedList<Integer> list = new LinkedList<>();
        int con = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase("X")) {
                con += 1;
            } else {
                if (con > 0) {
                    list.add(con);
                }
                con = 0;
            }
            
        }
        if (con > 0) {
            list.add(con);
        }
        
        return list;
    }
    
    
    
}
