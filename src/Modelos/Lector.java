/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jaiver
 */
public class Lector {

    final int SALTO_LINEA = 10;
    final int NUMERAL = 35;
    final int ESPACIO = 32;
    final int FIN_ARCHIVO = -1;

    public Nonograma leerArchivo() {
        FileReader fr = null;
        LinkedList<LinkedList> reglas = new LinkedList();
        try {
            fr = new FileReader("/home/jaiver/NetBeansProjects/ProyectoNonogramas/src/Archivos/datos.txt");
            try {
                int car = -2;
                LinkedList<String> regla = new LinkedList();
                while (car != FIN_ARCHIVO) {

                    car = fr.read();
                    if (car != NUMERAL) {
                        if (car != SALTO_LINEA && car != FIN_ARCHIVO) {
                            if (car != ESPACIO) {
                                char cad = (char) car;
                                regla.add(cad + "");
                            }
                        } else {
                            if (!regla.isEmpty()) {
                                reglas.add(regla);
                                regla = new LinkedList<>();
                            }
                        }
                    }

                }
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

        for (LinkedList regla : reglas) {
            for (Object object : regla) {
                System.out.print(object.toString() + " ");
            }
            System.out.println("");
        }
        
        int incremento = 1;
        if ((reglas.size() / 2) % 2 == 0) {
            incremento = 0;
        }
        
        LinkedList<LinkedList> reglasFil = new LinkedList<>();
        for(int i = 0; i < reglas.size() / 2; i++){
            int limit = incremento + (int)(Math.ceil((reglas.size() / 2) / 2));
            //System.out.println(limit+"<--");
            while(reglas.get(i).size() < limit){
                reglas.get(i).addFirst("");
            }
            
            reglasFil.add(reglas.get(i));
        }
        
        LinkedList<LinkedList> reglasCol = new LinkedList<>();
        for(int i = reglas.size() / 2; i < reglas.size(); i++){
            //reglas.get(i).addFirst("");
            //reglas.get(i).addFirst("");
            
            int limit = incremento + (int)(Math.ceil((reglas.size() / 2) / 2));
            while(reglas.get(i).size() < limit){
                reglas.get(i).addFirst("");
            }
            
            reglasCol.add(reglas.get(i));
        }
        
        return new Nonograma(new Nonograma().crearMatrizPruebas(reglas.size() / 2), reglasCol, reglasFil);
    }

}
