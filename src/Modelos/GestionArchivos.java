/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.*;
import java.util.LinkedList;

/**
 *
 * @author alda1
 */
public class GestionArchivos {
    
    public Nonograma Leer(String ruta) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        Nonograma nonograma = null;
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(ruta);
            //archivo = new File("src/Archivos/reglasNonograma.txt");
            
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            nonograma = cargarReglasDeNonograma(fr, br);
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        
        return nonograma;
    }
    
    public Nonograma LeerNonogramaResuelto(String ruta) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        Nonograma nonograma = null;
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            
            archivo = new File(ruta);
            
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            nonograma = cargarNonogramaResuelto(fr, br);
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        
        return nonograma;
    }
    
    public Nonograma cargarNonogramaResuelto(FileReader fr, BufferedReader br) throws IOException {
        AnalizadorDeEstructuras ae=new AnalizadorDeEstructuras();
        String linea;
        Nonograma nonograma = new Nonograma();
        LinkedList<String[]> list = new LinkedList<>();
        while ((linea = br.readLine()) != null) {
            if (!linea.contains("#")) {
                list.add(ae.partir(linea));
                
            }
        }
        nonograma.setMatriz(ae.convListMatriz(list));
        LinkedList<LinkedList<LinkedList<Integer>>> reglas=ae.AnalizarMatriz(nonograma.getMatriz());
        nonograma.setReglasFil(reglas.get(0));
        nonograma.setReglasCol(reglas.get(1));
        ae.formatearMatriz(nonograma);
        return nonograma;
    }
    
    
    
   
    
   
    
    
    public Nonograma cargarReglasDeNonograma(FileReader fr, BufferedReader br) throws IOException {
        AnalizadorDeEstructuras ae=new AnalizadorDeEstructuras();
        String linea;
        Nonograma nonograma = new Nonograma();
        int n = -1;
        int m = -1;
        
        while ((linea = br.readLine()) != null) {
            if (!linea.contains("#")) {
                if (n == -1) {
                    n = Integer.parseInt(linea);
                    for (int i = 0; i < n; i++) {
                        linea = br.readLine();
                        String[] s = linea.split(" ");
                        LinkedList<Integer> list = ae.StrinArrList(s);
                        nonograma.getReglasFil().add(list);
                        //     System.out.println(s[0]);
                    }
                    
                } else if (m == -1) {
                    m = Integer.parseInt(linea);
                    for (int i = 0; i < m; i++) {
                        linea = br.readLine();
                        String[] s = linea.split(" ");
                        LinkedList<Integer> list = ae.StrinArrList(s);
                        nonograma.getReglasCol().add(list);
                        //   System.out.println(s[0]);
                    }
                    
                }
            }
        }
        nonograma.crearMatriz(nonograma.getReglasFil().size(),nonograma.getReglasCol().size());
        return nonograma;
    }
    
    

    /*public static void main(String[] arg) {
        Nonograma no = Leer(1);
        System.out.println(no.getReglasFil());
        System.out.println(no.getReglasCol());
        //AnalizarMatriz(no.getMatriz());
    }*/
   
    
}
