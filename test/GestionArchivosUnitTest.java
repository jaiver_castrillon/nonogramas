/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelos.GestionArchivos;
import Modelos.Nonograma;
import Modelos.Procesar;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jaiver
 */
public class GestionArchivosUnitTest {
    
    public GestionArchivosUnitTest() {
    }
    
    
    @Test
    public void validarLecturaMatrizNonograma() {
        Nonograma nonograma = null;
        
        String[][] matriz = {
            {"X", "X", "X" , "X", ""},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"X", "X", "" , "", "X"}
        };
        
        
        nonograma = new GestionArchivos().Leer("src/Archivos/reglasNonograma_1.txt");
        
        Procesar ps = new Procesar();
        ps.resolver(nonograma);
        
        for (String[] strings : nonograma.getMatriz()) {
            for (String string : strings) {
                System.out.print("-"+string);
            }
            System.out.println("");
        }
        
        
        assertArrayEquals(matriz, nonograma.getMatriz());
        
    }
    
    
    @Test
    public void validarLecturaReglasNonograma() {
        Nonograma nonograma = new Nonograma();
        
        LinkedList<LinkedList<Integer>> filas = new LinkedList();
        
        LinkedList<Integer> fila1 = new LinkedList<>();
        fila1.add(4);
        
        LinkedList<Integer> fila2 = new LinkedList<>();
        fila2.add(2);
        
        LinkedList<Integer> fila3 = new LinkedList<>();
        fila3.add(2);
        
        LinkedList<Integer> fila4 = new LinkedList<>();
        fila4.add(2);
        
        LinkedList<Integer> fila5 = new LinkedList<>();
        fila5.add(2);
        fila5.add(1);
        
        filas.add(fila1);
        filas.add(fila2);
        filas.add(fila3);
        filas.add(fila4);
        filas.add(fila5);
        
        nonograma.setReglasFil(filas);
        
        LinkedList<LinkedList<Integer>> cols = new LinkedList();
        
        LinkedList<Integer> col1 = new LinkedList<>();
        col1.add(1);
        col1.add(1);
        
        LinkedList<Integer> col2 = new LinkedList<>();
        col2.add(1);
        col2.add(1);
        
        LinkedList<Integer> col3 = new LinkedList<>();
        col3.add(1);
        
        LinkedList<Integer> col4 = new LinkedList<>();
        col4.add(4);
        
        LinkedList<Integer> col5 = new LinkedList<>();
        col5.add(4);
        
        cols.add(col1);
        cols.add(col2);
        cols.add(col3);
        cols.add(col4);
        cols.add(col5);
        
        nonograma.setReglasCol(cols);
        
        nonograma.crearMatriz(5, 5);
        
        //Procesar ps = new Procesar();
        //ps.resolver(nonograma);
        
        String[][] matriz = {
            {"X", "X", "X" , "X", ""},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"X", "X", "" , "", "X"}
        };
        
        nonograma = new GestionArchivos().Leer("src/Archivos/reglasNonograma_1.txt");
        
        assertEquals(concatenarListas(filas), concatenarListas(nonograma.getReglasFil()));
        assertEquals(concatenarListas(cols), concatenarListas(nonograma.getReglasCol()));
        
    }    
    
    
    
    public String concatenarListas(LinkedList<LinkedList<Integer>> lista){
        String cad = "";
        
        for (LinkedList linkedList : lista) {
            for (Object object : linkedList) {
                cad += object;
            }
        }
        
        return cad;
    }
}
