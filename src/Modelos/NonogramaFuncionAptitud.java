package Modelos;

import java.util.ArrayList;
import java.util.LinkedList;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.IChromosome;

public class NonogramaFuncionAptitud extends FitnessFunction {

    private String meta;
    private LinkedList<LinkedList<Integer>> reglasCol;
    private LinkedList<LinkedList<Integer>> reglasFil;

    
   
    
    public void setTarget(LinkedList<LinkedList<Integer>> reglasCol,LinkedList<LinkedList<Integer>> reglasFil) {
        //this.meta = meta;
        this.reglasFil=reglasFil;
        this.reglasCol=reglasCol;
    }

    @Override
    protected double evaluate(IChromosome individuo) {
        int errores = 0;
        String [][] mat=formatearMatriz(individuo.getGenes(), this.reglasFil.size(), this.reglasFil.size());
        String[] arr = new String[mat.length];
        for (int i = 0; i < mat.length; i++) {
            if(!CompararListas(this.reglasFil.get(i), ExtraerReglas(mat[i])))
            {
              errores=errores+1;
            }
            for (int j = 0; j < mat.length; j++) {
                arr[j] = mat[j][i];
             //   System.out.println(arr[j]);
            }
            if(!CompararListas(this.reglasFil.get(i), ExtraerReglas(arr)))
            {
              errores=errores+1;
            }
        }
        return errores;
    }
    public String[][] formatearMatriz(Gene[] cad,int fil,int col)
    {
        String[][] mat=new String[fil][col];
        int ind=0;
        for (int i = 0; i < fil; i++) {
            for (int j = 0; j < col; j++) {
                mat[i][j]=(String) cad[ind].getAllele();
                ind=ind+1; 
            }
        }
    
    return mat;
    }
    
    public LinkedList<Integer> ExtraerReglas(String[] arr) {
        LinkedList<Integer> list = new LinkedList();
        int con = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase("1")) {
                con += 1;
            } else {
                if (con > 0) {
                    list.add(con);
                }
                con = 0;
            }
            
        }
        if (con > 0) {
            list.add(con);
        }   
        return list;
    }
    
    public boolean CompararListas(LinkedList<Integer> list1, LinkedList<Integer> list2) {
        if (list1.size() == list2.size()) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i) != list2.get(i)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
    
}
