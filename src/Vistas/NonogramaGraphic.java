/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelos.Lector;
import Modelos.Nonograma;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;

/**
 *
 * @author jaiver
 */
public class NonogramaGraphic extends javax.swing.JPanel {

    Nonograma nonograma;
    final int INICIO_X = 10;
    final int INICIO_Y = 10;
    final int ANCHO_CELDA = 20;

    /**
     * Creates new form NonogramaGraphic
     */
    public NonogramaGraphic() {
        //this.nonograma = new Nonograma();
        initComponents();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
        paintNonograma(g);
        repaint();
    }

    public void paintNonograma(Graphics g) {
        if (this.nonograma != null) {
            int incremento = 1;
            if (this.nonograma.getMatriz().length % 2 == 0) {
                incremento = 0;
            }

            int longReglasMed = ((int) Math.ceil((this.nonograma.getMatriz().length) / 2) + incremento) * ANCHO_CELDA;
//            int longReglasMed = this.nonograma.getReglasCol().get(0).size() * ANCHO_CELDA;
            
            int longReglasCol = 0;
            int longReglasFil = 0;
                    
            
            //longReglasCol = this.nonograma.getReglasCol().get(0).size() * ANCHO_CELDA;
            //longReglasFil = this.nonograma.getReglasFil().get(0).size() * ANCHO_CELDA;
            
            longReglasCol = this.nonograma.sublistaMayor(this.nonograma.getReglasCol()) * ANCHO_CELDA;
            longReglasFil = this.nonograma.sublistaMayor(this.nonograma.getReglasFil()) * ANCHO_CELDA;
                    
            
            
            if(longReglasCol > longReglasFil){
                longReglasFil = longReglasCol;
            }else{
                longReglasCol = longReglasFil;
            }
            
            int longReglas = (int) this.nonograma.getMatriz().length * ANCHO_CELDA;
            
            Rectangle miniGrafico = new Rectangle(INICIO_X, INICIO_Y, longReglasMed, longReglasMed);
            Rectangle rectReglasFil = new Rectangle(INICIO_X + longReglasFil, INICIO_Y, longReglas, longReglasFil);
            Rectangle rectReglasCol = new Rectangle(INICIO_X, INICIO_Y + longReglasCol, longReglasCol, longReglas);

            g.setColor(Color.black);

            Point inicioReglasCol = new Point(rectReglasCol.x, rectReglasCol.y);
            Point inicioReglasFil = new Point(rectReglasFil.x, rectReglasFil.y);
            //Point inicioMatriz = new Point(miniGrafico.x + miniGrafico.height, miniGrafico.y + miniGrafico.height);
            Point inicioMatriz = new Point(rectReglasFil.x, rectReglasCol.y);
            
//            Point inicioReglasCol = new Point(miniGrafico.x, miniGrafico.y + miniGrafico.height);
//            Point inicioReglasFil = new Point(miniGrafico.x + miniGrafico.height, miniGrafico.y);
//            Point inicioMatriz = new Point(miniGrafico.x + miniGrafico.height, miniGrafico.y + miniGrafico.height);
            
            g.setColor(Color.LIGHT_GRAY);
            graficarReglasFil(inicioReglasCol, g);
            graficarReglasCol(inicioReglasFil, g);
            g.setColor(Color.gray);
            graficarMatriz(inicioMatriz, g);

            g.setColor(Color.BLUE);
            //GRAFICA ESPACIO DE REGLAS DE LAS FILAS.
            g.drawRect(rectReglasFil.x, rectReglasFil.y, rectReglasFil.width, rectReglasFil.height);
            //g.drawRect(miniGrafico.x, miniGrafico.y + miniGrafico.height, miniGrafico.height, longReglas);

            //GRAFICA ESPACIO DE REGLAS DE LAS COLUMNAS.
            g.drawRect(rectReglasCol.x, rectReglasCol.y, rectReglasCol.width, rectReglasCol.height);
            //g.drawRect(miniGrafico.x + miniGrafico.height, miniGrafico.y, longReglas, miniGrafico.height);

            //GRAFICA ESPACIO DE LA MATRIZ.
            //g.drawRect(miniGrafico.x + miniGrafico.height, miniGrafico.y + miniGrafico.height, longReglas, longReglas);
            
            g.drawRect(rectReglasFil.x, rectReglasCol.y, longReglas, longReglas);

            //g.setColor(Color.white);
            //g.fillRect(miniGrafico.x, miniGrafico.y, miniGrafico.height, miniGrafico.height);
        }
    }

    public void graficarReglasFil(Point inicio, Graphics g) {
        int incremento = 1;
        if (this.nonograma.getMatriz().length % 2 == 0) {
            incremento = 0;
        }
        //g.drawOval(inicio.x, inicio.y, 5, 5);
        //System.out.println("-->" + this.nonograma.sublistaMayor(this.nonograma.getReglasFil()));
        
        //for (int i = this.nonograma.getMatriz().length - 1; i > -1; i--) {
        for (int i = 0; i < this.nonograma.getMatriz().length; i++) {    
            
            for (int j = 0; j < this.nonograma.getReglasFil().get(i).size(); j++) {
            //for (int j = this.nonograma.getReglasFil().get(i).size() ; j > 0; j--) {    
                
                int y = (i * ANCHO_CELDA) + inicio.y;
                int x = (j * ANCHO_CELDA) + inicio.x;
//                int x = (j * ANCHO_CELDA) + inicio.x + ((j-1) * ANCHO_CELDA);
                

                g.drawRect(x, y, ANCHO_CELDA, ANCHO_CELDA);
                String cad = this.nonograma.getReglasFil().get(i).get(j).toString();
                g.setColor(Color.BLACK);
                g.drawString(cad, x + (ANCHO_CELDA / 4), y + (ANCHO_CELDA / 2 + (ANCHO_CELDA / 4)));
                g.setColor(Color.LIGHT_GRAY);
            }
        }

    }

    public void graficarReglasCol(Point inicio, Graphics g) {
        int incremento = 1;
        if (this.nonograma.getMatriz().length % 2 == 0) {
            incremento = 0;
        }
        //g.drawOval(inicio.x, inicio.y, 5, 5);

        for (int i = 0; i < this.nonograma.getMatriz().length; i++) {
            for (int j = 0; j < this.nonograma.getReglasCol().get(i).size(); j++) {

//            for (int j = 0; j < incremento + (int) (Math.ceil((this.nonograma.getMatriz().length) / 2)); j++) {
                int x = (i * ANCHO_CELDA) + inicio.x;
                int y = (j * ANCHO_CELDA) + inicio.y;

                g.drawRect(x, y, ANCHO_CELDA, ANCHO_CELDA);
                String cad = this.nonograma.getReglasCol().get(i).get(j).toString();
                g.setColor(Color.BLACK);
                g.drawString(cad, x + (ANCHO_CELDA / 4), y + (ANCHO_CELDA / 2 + (ANCHO_CELDA / 4)));
                g.setColor(Color.LIGHT_GRAY);

            }
        }

    }

    public void graficarMatriz(Point inicio, Graphics g) {
//        printMatriz();
        int incremento = 1;
        if (this.nonograma.getMatriz().length % 2 == 0) {
            incremento = 0;
        }
        //g.drawOval(inicio.x, inicio.y, 5, 5);

        for (int i = 0; i < this.nonograma.getMatriz().length; i++) {
            for (int j = 0; j < this.nonograma.getMatriz()[0].length; j++) {

                int x = (i * ANCHO_CELDA) + inicio.x;
                int y = (j * ANCHO_CELDA) + inicio.y;

                //g.drawRect(x, y, ANCHO_CELDA, ANCHO_CELDA);
//                g.drawString(cad, x + (ANCHO_CELDA / 4), y + (ANCHO_CELDA / 2 + (ANCHO_CELDA / 4)));
//                System.out.println(this.nonograma.getMatriz()[i][j]);    
                if (!this.nonograma.getMatriz()[i][j].equals("")) {
                    g.fillRect(y, x, ANCHO_CELDA, ANCHO_CELDA);
                } else {
                    g.drawRect(y, x, ANCHO_CELDA, ANCHO_CELDA);
                }
            }
        }
        int ancho_celda = 3;
        g.drawRect(INICIO_X, INICIO_Y, ancho_celda * this.nonograma.getMatriz()
                .length, ancho_celda * this.nonograma.getMatriz().length);
        
        for (int i = 0; i < this.nonograma.getMatriz().length; i++) {
            for (int j = 0; j < this.nonograma.getMatriz()[0].length; j++) {
                
                
                
                int x = (i * ancho_celda) + INICIO_X;
                int y = (j * ancho_celda) + INICIO_Y;

                if (!this.nonograma.getMatriz()[i][j].equals("")) {
                    g.fillRect(y, x, ancho_celda, ancho_celda);
                } 
            }
        }

    }

    public void printMatriz() {
        System.out.println("MATRIZ-----------------------------------------");
        for (int i = 0; i < this.nonograma.getMatriz().length; i++) {
            for (int j = 0; j < this.nonograma.getMatriz()[i].length; j++) {
                System.out.print(this.nonograma.getMatriz()[i][j] + " - ");
            }
            System.out.println("");
        }
        System.out.println("REGLAS FIL-----------------------------------------");
        
        for (LinkedList linkedList : this.nonograma.getReglasFil()) {
            for (Object object : linkedList) {
                System.out.print(" - " + object);
            }
            System.out.println("");
        }
        
        System.out.println("REGLAS COL-----------------------------------------");
        
        for (LinkedList linkedList : this.nonograma.getReglasCol()) {
            for (Object object : linkedList) {
                System.out.print(" - " + object);
            }
            System.out.println("");
        }
        System.out.println("-----------------------------------------");
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(java.awt.Color.white);
        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
