/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author alda1
 */
public class Individuo  implements Comparable<Individuo>{
    
    private String[][] cromosoma;
    private int aptitud;
    
    public Individuo(String[][] cromosoma) {
        this.cromosoma = cromosoma;   
    }
    
    
    public Individuo crearGenoma(int n, int m) {
        String[][] cromosoma = new String[m][m];
        for (int i = 0; i < cromosoma.length; i++) {
            for (int j = 0; j < cromosoma[0].length; j++) {
                cromosoma[i][j]=Mutar();
            }
        }
        
        return new Individuo(cromosoma);
    }
    
    public String Mutar() {
        if (Math.random() < 0.5) {
            return "";
        }
        {
            return "X";
        }
    }
    public Individuo reproducir(String[][] madre) {
        String[][] hijo = new String[madre.length][madre[0].length];
        double ran = 0;
        for (int i = 0; i < madre.length; i++) {
            for (int j = 0; j < madre[0].length; j++) {
                ran = Math.random();
                
                if (ran < 0.54) {
                    hijo[i][j] = this.cromosoma[i][j];
                } else if (ran < 0.96) {
                    hijo[i][j] = madre[i][j];
                } else {
                    hijo[i][j] = Mutar();
                }
                
            }
        }
        return new Individuo(hijo);
    }

    /**
     * @return the cromosoma
     */
    public String[][] getCromosoma() {
        return cromosoma;
    }

    /**
     * @param cromosoma the cromosoma to set
     */
    public void setCromosoma(String[][] cromosoma) {
        this.cromosoma = cromosoma;
    }

    /**
     * @return the aptitud
     */
    public int getAptitud() {
        return aptitud;
    }

    /**
     * @param aptitud the aptitud to set
     */
    public void setAptitud(int aptitud) {
        this.aptitud = aptitud;
    }

    @Override
    public int compareTo(Individuo ind2) {
  
        return this.aptitud-ind2.getAptitud();
        /*if(this.aptitud<ind2.getAptitud())
        {
            return -1;
        
        }else if(this.aptitud>ind2.getAptitud())
        {
            return 1;
        
        }else{
        
        return 0;
        }*/
    }

    
    
}
