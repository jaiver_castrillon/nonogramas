/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelos.Nonograma;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jaiver
 */
public class NonogramaUnitTest {

    public NonogramaUnitTest() {
    }

    @Before
    public void setUp() {
    }

    @Test
    public void crearMatrizUnitTest() {
        String[][] matriz = {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}};
        Nonograma nonograma = new Nonograma();
        nonograma.crearMatriz(4, 4);
        assertArrayEquals(matriz, nonograma.getMatriz());
    }

    @Test
    public void sublistaMayorUnitTest() {
        LinkedList<LinkedList<Integer>> lista = new LinkedList();
        
        LinkedList<Integer> sublista1 = new LinkedList<>();
        sublista1.add(1);
        sublista1.add(2);
        
        LinkedList<Integer> sublista2 = new LinkedList<>();
        sublista2.add(1);
        sublista2.add(2);
        sublista2.add(3);
        lista.add(sublista1);
        lista.add(sublista2);
        System.out.println(new Nonograma().sublistaMayor(lista));
        assertEquals(3, new Nonograma().sublistaMayor(lista));
    }

}
