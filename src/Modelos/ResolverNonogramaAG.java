package Modelos;

import java.util.LinkedList;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.DeltaFitnessEvaluator;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.StringGene;

public class ResolverNonogramaAG {

    //private static final String META = "0101010010011111";
    private static final int EVOLUCIONES = 300000;
    

    public ResolverNonogramaAG(LinkedList<LinkedList<Integer>> reglasFil,LinkedList<LinkedList<Integer>> reglasCol) throws Exception {
        Genotype genotipo = this.setupGenoType(reglasFil,reglasCol);
        this.evolucionar(genotipo);
    }

    private void evolucionar(Genotype genotipo) {
        String solucion = this.getSolucion(genotipo.getFittestChromosome());
        System.out.println(solucion);

        double fitnessPrevio = Double.MAX_VALUE;
        int numEvoluciones = 0;
        for (int i = 0; i < EVOLUCIONES; i++) {
            genotipo.evolve();
            double fitness = genotipo.getFittestChromosome().getFitnessValue();
            System.out.println(fitness);
            if (fitness < fitnessPrevio) {
                fitnessPrevio = fitness;
                solucion = this.getSolucion(genotipo.getFittestChromosome());
                System.out.println(solucion);
            }
            if (fitness==0.0) {
                numEvoluciones = i;
                break;
            }
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
            }
        }
        solucion = this.getSolucion(genotipo.getFittestChromosome());
        System.out.println(solucion);
        System.out.println("Se necesitaron [" + numEvoluciones + "] evoluciones.");
    }

    private String getSolucion(IChromosome a_subject) {
        StringBuilder solucion = new StringBuilder();
        Gene[] genes = a_subject.getGenes();
        String alelo;
        for (Gene gen : genes) {
            alelo = (String) gen.getAllele();
            solucion.append(alelo);
        }
        return solucion.toString();
    }

    private Genotype setupGenoType(LinkedList<LinkedList<Integer>> reglasFil,LinkedList<LinkedList<Integer>> reglasCol) throws Exception {
        Configuration gaConf = new DefaultConfiguration();
        gaConf.resetProperty(Configuration.PROPERTY_FITEVAL_INST);
        gaConf.setFitnessEvaluator(new DeltaFitnessEvaluator());

        gaConf.setPreservFittestIndividual(true);
        gaConf.setKeepPopulationSizeConstant(false);

        gaConf.setPopulationSize(1000);

        int tamCromosomas = reglasCol.size()*reglasFil.size();

        StringGene gen = new StringGene(gaConf);
        gen.setMaxLength(1);
        gen.setMinLength(1);
        gen.setAlphabet("01");

        IChromosome cromosomaEjemplo = new Chromosome(gaConf, gen, tamCromosomas);
        gaConf.setSampleChromosome(cromosomaEjemplo);

        NonogramaFuncionAptitud funcionAptitud = new NonogramaFuncionAptitud();
        funcionAptitud.setTarget(reglasFil,reglasCol);
        gaConf.setFitnessFunction(funcionAptitud);

        return Genotype.randomInitialGenotype(gaConf);
    }

    public static void main(String[] args) throws Exception {
        GestionArchivos ga=new GestionArchivos();
        Nonograma no=ga.Leer("src/Archivos/reglasNonograma.txt");
        ResolverNonogramaAG res=new ResolverNonogramaAG(no.getReglasCol(), no.getReglasFil());
        
    }
    
   
}
