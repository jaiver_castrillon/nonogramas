/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelos.GestionArchivos;
import Modelos.Nonograma;
import Modelos.Procesar;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jaiver
 */
public class ProcesarUnitTest {
    
    public ProcesarUnitTest() {
    }
    
    
    @Test
    public void validarResolucionNonograma() {
        Nonograma nonograma = null;
        
        String[][] matriz = {
            {"X", "X", "X" , "X", ""},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"", "", "", "X" , "X"},
            {"X", "X", "" , "", "X"}
        };
        
        
        nonograma = new GestionArchivos().Leer("src/Archivos/reglasNonograma_1.txt");
        
        Procesar ps = new Procesar();
        ps.resolver(nonograma);
        
        for (String[] strings : nonograma.getMatriz()) {
            for (String string : strings) {
                System.out.print("-"+string);
            }
            System.out.println("");
        }
        
        
        assertArrayEquals(matriz, nonograma.getMatriz());
        
    }
}
