package Modelos;

import Vistas.Visualizador;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;

/**
 * Agente qué hará las veces de cliente.
 *
 * @author jaiver
 */
public class AgenteEmisor extends GuiAgent {

    private int comando = -1;
    transient protected Visualizador emisorGUI;

    /**
     * El agente por defecto instanciará y mostrará la interfaz para la solución
     * de nonogramas.
     */
    @Override
    public void setup() {
        emisorGUI = new Visualizador(this);
        emisorGUI.setVisible(true);

    }

    /**
     * Asociamos un evento de un elemento de la interfaz hacia este método, para
     * que cada vez que se accione ese elemento se ejecute esta función.
     *
     * @param ge GuiEvent para hacer diferentes acciones.
     */
    @Override
    protected void onGuiEvent(GuiEvent ge) {
        comando = ge.getType();
        if (comando == 1) {
            enviarMensaje();
        }

    }

    /**
     * Configura y envía el mensaje de petición de resolución de nonograma al
     * agente servidor.
     */
    public void enviarMensaje() {
        AID id = new AID();
        //id.setLocalName(emisorGUI.getDestinatario());
        //Agente que recibe el mensaje, siempre será el server.
        id.setLocalName("server");
        ACLMessage mensaje = new ACLMessage(ACLMessage.REQUEST);
        mensaje.setSender(getAID());
        mensaje.setLanguage("Español");
        mensaje.addReceiver(id);
        mensaje.setContent("Resuelve el nonograma");
        send(mensaje);
        addBehaviour(new EnviaSolicitud());

    }

    /**
     * Clase encargada de enviar mensajes a un agente.
     */
    class EnviaSolicitud extends SimpleBehaviour {

        /**
         * Action específica que envía el mensaje y recoge la respuesta a ese
         * mensaje.
         */
        @Override
        public void action() {
            ACLMessage mensajeRespuesta = blockingReceive();
            if (mensajeRespuesta != null) {
                System.out.println("Soy cliente(" + getLocalName() + ") --> "
                        + "recibí respuesta de:" + mensajeRespuesta.getSender().getLocalName()
                        + "; mensaje: " + mensajeRespuesta.getContent());
            }
        }

        @Override
        public boolean done() {
            return true;
        }

    }

}
