package Modelos;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * Agente que funcionará como servidor, recibirá las peticiones para resolver
 * nonogramas.
 *
 * @author jaiver
 */
public class AgenteReceptor extends Agent {

    /**
     * En la configuración de este agente, iniciamos el ciclo para estar
     * constantemente preguntando si le llegan mensajes.
     */
    @Override
    public void setup() {
        RecibeSolicitud recibe = new RecibeSolicitud();
        addBehaviour(recibe);
    }

    /**
     * Clase encargada de recorrer el ciclo de lectura.
     */
    class RecibeSolicitud extends CyclicBehaviour {
        
        /**
         * Action específica que consulta su log de mensajes y analiza que 
         * mensaje recibe para resolver nonogramas.
         */
        @Override
        public void action() {
            ACLMessage mensaje = receive();
            if (mensaje != null) {
                System.out.println("Soy el server, recibe mensaje de:"
                        + mensaje.getSender().getLocalName() + "; mensaje: "
                        + mensaje.getContent());
                ACLMessage respuesta = mensaje.createReply();
                respuesta.setPerformative(ACLMessage.INFORM);
                respuesta.setContent("Mensaje recibido.");
                send(respuesta);

            }

        }

    }
}
