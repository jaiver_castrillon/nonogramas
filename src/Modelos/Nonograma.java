package Modelos;

import java.util.LinkedList;

/**
 *
 * @author jaiver
 */
public class Nonograma {

    private String[][] matriz;
    private LinkedList<LinkedList<Integer>> reglasCol;
    private LinkedList<LinkedList<Integer>> reglasFil;

    public Nonograma() {
        //setMatriz();
        reglasCol = new LinkedList<>();
        reglasFil = new LinkedList<>();

    }

    public Nonograma(String[][] matriz, LinkedList reglasCol, LinkedList reglasFil) {
        this.matriz = matriz;
        this.reglasCol = reglasCol;
        this.reglasFil = reglasFil;

    }

    public void setMatriz() {

        /*this.reglasCol = new LinkedList<>();
        this.reglasCol.add(getListaTemp());
        this.reglasCol.add(getListaTemp());
        this.reglasCol.add(getListaTemp());
        this.reglasCol.add(getListaTemp());
        this.reglasCol.add(getListaTemp());

        this.reglasFil = new LinkedList<>();
        this.reglasFil.add(getListaTemp());
        this.reglasFil.add(getListaTemp());
        this.reglasFil.add(getListaTemp());
        this.reglasFil.add(getListaTemp());
        this.reglasFil.add(getListaTemp());

        this.matriz = crearMatrizPruebas(5);
*/
        //this.matriz[3][2] = "-";
    }

    public LinkedList<String> getListaTemp() {
        LinkedList<String> temp1 = new LinkedList<>();
        temp1.add("1");
        temp1.add("2");
        temp1.add("3");
        return temp1;
    }

    public void crearMatriz(int filas, int columnas) {
        String[][] mat = new String[filas][columnas];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                mat[i][j] = "";
            }
        }
        this.matriz = mat;
    }

    public String[][] crearMatrizPruebas(int tam) {
        String[][] mat = new String[tam][tam];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                mat[i][j] = "";
            }
        }
        return mat;
    }

    public void formatearReglas() {
        int nroMayorVacios = reglasCol.size();
        for (LinkedList lista : reglasCol) {
            int nroMayorTemp = 0;
            for (Object cad : lista) {
                if (cad.equals("")) {
                    nroMayorTemp++;
                }
            }
            if (nroMayorTemp <= nroMayorVacios) {
                nroMayorVacios = nroMayorTemp;
            }
        }
        eliminarVacios(reglasCol, nroMayorVacios);
        nroMayorVacios = reglasFil.size();
        for (LinkedList lista : reglasFil) {
            int nroMayorTemp = 0;
            for (Object cad : lista) {
                if (cad.equals("")) {
                    nroMayorTemp++;
                }
            }
            if (nroMayorTemp <= nroMayorVacios) {
                nroMayorVacios = nroMayorTemp;
            }
        }
        System.out.println(reglasFil.get(0).size() + "<- fil");
        eliminarVacios(reglasFil, nroMayorVacios);

        System.out.println(reglasFil.get(0).size() + "<- fil");
        System.out.println(reglasCol.size() + "<- col");
        //System.out.println("nroMayorVacios" + nroMayorVacios);
    }

    public void eliminarVacios(LinkedList<LinkedList<Integer>> reglas, int cantidad) {
        for (LinkedList regla : reglas) {
            for (int i = 0; i < cantidad; i++) {
                regla.poll();
            }
        }
    }

    public int sublistaMayor(LinkedList<LinkedList<Integer>> lista) {
        int mayor = lista.get(0).size();

        for (LinkedList linkedList : lista) {
            if (linkedList.size() > mayor) {
                mayor = linkedList.size();
            }
        }
        return mayor;
    }
    public void llenarCamposOb()
    {
        for (int i = 0; i < this.reglasFil.size(); i++) {
            if(this.reglasFil.get(i).size()==1)
            {
                if((this.matriz[i].length-this.reglasFil.get(i).get(0))>(this.matriz[i].length/2))
                {
                    for (int j =this.matriz[i].length-this.reglasFil.get(i).get(0)-1; j <this.matriz[i].length-this.reglasFil.get(i).get(0); j++) {
                        
                    }
                }
            
            }
        }
    
    
    
    }
    public String[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(String[][] matriz) {
        this.matriz = matriz;
    }

    public LinkedList<LinkedList<Integer>> getReglasCol() {
        return reglasCol;
    }

    public void setReglasCol(LinkedList reglasCol) {
        this.reglasCol = reglasCol;
    }

    public LinkedList<LinkedList<Integer>> getReglasFil() {
        return reglasFil;
    }

    public void setReglasFil(LinkedList reglasFil) {
        this.reglasFil = reglasFil;
    }

}
